package imb4;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.io.OutputStream;
import java.util.UUID;

public class TEventEntry {
    // constructor
    public TEventEntry(int aEventID, String aEventName, TConnection aConnection) {
        fEventID = aEventID;
        fEventName = aEventName;
        fConnection = aConnection;

        fIsSubscribed = false;
        fIsPublished = false;
        fSubscribers = false;
        fPublishers = false;
    }

    // fields
    private int fEventID;
    private String fEventName;
    private TConnection fConnection;

    private boolean fIsSubscribed;
    private boolean fIsPublished;
    private boolean fSubscribers;
    private boolean fPublishers;

    private class TStreamCache {
        private String fStreamName;
        private OutputStream fStream;

        public TStreamCache(String aStreamName, OutputStream aStream) {
            fStreamName = aStreamName;
            fStream = aStream;
        }
    }

    private Map<UUID, TStreamCache> fStreamCache = new HashMap<>();

    // getters/setters
    public int getEventID() {
        return fEventID;
    }

    public String getfEventName() {
        return fEventName;
    }

    public boolean isSubscribed() {
        return fIsSubscribed;
    }

    public boolean isPublished() {
        return fIsPublished;
    }

    public boolean hasSubscribers() {
        return fSubscribers;
    }

    public boolean hasPublishers() {
        return fPublishers;
    }

    // other methods
    private void signalSubscribe() {
        fConnection.writeCommand(
                new TByteBuffer(
                        TByteBuffer.bb_tag_string(TConnection.icehEventName, fEventName),
                        TByteBuffer.bb_tag_uint32(TConnection.icehSubscribe, fEventID)),
                true);
    }

    private void signalPublish() {
        fConnection.writeCommand(
                new TByteBuffer(
                        TByteBuffer.bb_tag_string(TConnection.icehEventName, fEventName),
                        TByteBuffer.bb_tag_uint32(TConnection.icehPublish, fEventID)),
                true);
    }

    private void signalUnSubscribe() {
        fConnection.writeCommand(
                TByteBuffer.bb_tag_uint32(TConnection.icehUnsubscribe, fEventID),
                true);
    }

    private void signalUnPublish() {
        fConnection.writeCommand(
                TByteBuffer.bb_tag_uint32(TConnection.icehUnpublish, fEventID),
                true);
    }

    public TEventEntry Subscribe() {
        if (!fIsSubscribed) {
            signalSubscribe();
            fIsSubscribed = true;
        }
        return this;
    }

    public TEventEntry Publish() {
        if (!fIsPublished) {
            signalPublish();
            fIsPublished = true;
        }
        return this;
    }

    public TEventEntry UnSubscribe() {
        if (fIsSubscribed) {
            signalUnSubscribe();
            fIsSubscribed = false;
        }
        return this;
    }

    public TEventEntry UnPublish() {
        if (fIsPublished) {
            signalUnPublish();
            fIsPublished = false;
        }
        return this;
    }

    public void signalEvent(TByteBuffer aPayload) {
        Publish();
        TByteBuffer packet = new TByteBuffer(
                TByteBuffer.bb_byte(TConnection.imbMagic),
                TByteBuffer.bb_int32(2 + aPayload.remaining()),
                TByteBuffer.bb_uint16((short) fEventID),
                aPayload
        );
        fConnection.writePacket(packet, true);
    }

    public void signalEvent(TByteBuffer aPayload1, TByteBuffer aPayload2) {
        Publish();
        TByteBuffer packet = new TByteBuffer(
                TByteBuffer.bb_byte(TConnection.imbMagic),
                TByteBuffer.bb_int32(2 + aPayload1.remaining() + aPayload2.remaining()),
                TByteBuffer.bb_uint16((short) fEventID),
                aPayload1,
                aPayload2
        );
        fConnection.writePacket(packet, true);
    }

    public void signalEvent(TByteBuffer aPayload1, TByteBuffer aPayload2, TByteBuffer aPayload3) {
        Publish();
        TByteBuffer packet = new TByteBuffer(
                TByteBuffer.bb_byte(TConnection.imbMagic),
                TByteBuffer.bb_int32(2 + aPayload1.remaining() + aPayload2.remaining() + aPayload3.remaining()),
                TByteBuffer.bb_uint16((short) fEventID),
                aPayload1,
                aPayload2,
                aPayload3
        );
        fConnection.writePacket(packet, true);
    }

    public void signalChangeObject(int aAction, int aObjectID, String aAttribute) {
        signalEvent(
                TByteBuffer.bb_tag_int32(TConnection.icehChangeObjectAction, aAction),
                TByteBuffer.bb_tag_string(TConnection.icehChangeObjectAttribute, aAttribute),
                TByteBuffer.bb_tag_int32(TConnection.icehChangeObject, aObjectID)
        );
    }

    public void signalString(String aString)
    {
        signalEvent(
                TByteBuffer.bb_tag_string(TConnection.icehString, aString)
        );
    }

    public void signalIntString(int aInt, String aString)
    {
        signalEvent(
                TByteBuffer.bb_tag_string(TConnection.icehIntStringPayload, aString),
                TByteBuffer.bb_tag_int32(TConnection.icehIntString, aInt)
        );
    }

    public void signalStream(String aName, InputStream aStream)
    {
        TByteBuffer bufferStreamID = TByteBuffer.bb_tag_guid(TConnection.icehStreamID, UUID.randomUUID());
        // header
        signalEvent(bufferStreamID, TByteBuffer.bb_tag_string(TConnection.icehStreamHeader, aName));
        // body
        byte[] buffer = new byte[TConnection.imbMaxStreamBodyBuffer];

        int readSize;
        try {
            readSize = aStream.read(buffer);
            while (readSize>0)
            {
                bufferStreamID.position(0);
                signalEvent(bufferStreamID, TByteBuffer.bb_tag_bytes(TConnection.icehStreamBody, buffer, 0, readSize));
                readSize = aStream.read(buffer);
            }
            // end
            bufferStreamID.position(0);
            signalEvent(bufferStreamID, TByteBuffer.bb_tag_bool(TConnection.icehStreamEnd, false));
        } catch (IOException e) {
            // end: cancel
            bufferStreamID.position(0);
            signalEvent(bufferStreamID, TByteBuffer.bb_tag_bool(TConnection.icehStreamEnd, true));
        }
    }

    public void handleEvent(TByteBuffer aPayload) throws Exception {
        String command_payload = "";
        int action = -1;
        String attribute = "";
        UUID stream_id = null;
        while (aPayload.remaining() > 0) {
            int fieldInfo = aPayload.bb_read_uint32();
            if (fieldInfo == (TConnection.icehIntString << 3 | TByteBuffer.wtVarInt)) {
                int command = aPayload.bb_read_int32();
                if (onIntStringEvent != null)
                    onIntStringEvent.dispatch(this, command, command_payload);
            } else if (fieldInfo == (TConnection.icehIntStringPayload << 3 | TByteBuffer.wtLengthDelimited)) {
                command_payload = aPayload.bb_read_string();
            } else if (fieldInfo == (TConnection.icehString << 3 | TByteBuffer.wtLengthDelimited)) {
                String command = aPayload.bb_read_string();
                if (onStringEvent != null)
                    onStringEvent.dispatch(this, command);
            } else if (fieldInfo == (TConnection.icehChangeObjectAction << 3 | TByteBuffer.wtVarInt)) {
                action = aPayload.bb_read_int32();
            } else if (fieldInfo == (TConnection.icehChangeObjectAttribute << 3 | TByteBuffer.wtLengthDelimited)) {
                attribute = aPayload.bb_read_string();
            } else if (fieldInfo == (TConnection.icehChangeObject << 3 | TByteBuffer.wtVarInt)) {
                int object_id = aPayload.bb_read_int32();
                if (onChangeObject != null)
                    onChangeObject.dispatch(this, action, object_id, attribute);
            } else if (fieldInfo == (TConnection.icehStreamHeader << 3 | TByteBuffer.wtLengthDelimited)) {
                String stream_name = aPayload.bb_read_string();
                if (onStreamCreate != null) {
                    OutputStream stream = onStreamCreate.dispatch(this, stream_name);
                    if (stream != null)
                        fStreamCache.put(stream_id, new TStreamCache(stream_name, stream));
                }
            } else if (fieldInfo == (TConnection.icehStreamBody << 3 | TByteBuffer.wtLengthDelimited)) {
                TStreamCache sc = fStreamCache.get(stream_id);
                if (sc != null && sc.fStream != null)
                    sc.fStream.write(aPayload.bb_read_bytes());
                else
                    aPayload.bb_read_skip(TByteBuffer.wtLengthDelimited);
            } else if (fieldInfo == (TConnection.icehStreamEnd << 3 | TByteBuffer.wtVarInt)) {
                boolean cancel = aPayload.bb_read_bool();
                TStreamCache sc = fStreamCache.get(stream_id);
                if (sc != null && sc.fStream != null) {
                    if (onStreamEnd != null)
                            onStreamEnd.dispatch(this, sc.fStream, sc.fStreamName, cancel);
                    sc.fStream.close();
                }
                fStreamCache.remove(stream_id);
            } else if (fieldInfo == (TConnection.icehStreamID << 3 | TByteBuffer.wtLengthDelimited)) {
                stream_id = aPayload.bb_read_guid();
            } else {
                if (onTag != null) {
                    int pos = aPayload.position(); // store position before handler
                    onTag.dispatch(this, fieldInfo, aPayload);
                    aPayload.position(pos); // restore position (to skip over it again but we have to make sure correct number of bytes are read
                }
                aPayload.bb_read_skip(fieldInfo & 7);
            }

        }
    }

    public void handleSubAndPub(int aCommand) {
        switch (aCommand) {
            case TConnection.icehSubscribe:
                fSubscribers = true;
                break;
            case TConnection.icehPublish:
                fPublishers = true;
                break;
            case TConnection.icehUnsubscribe:
                fSubscribers = false;
                break;
            case TConnection.icehUnpublish:
                fPublishers = false;
                break;
        }
        if (onSubAndPub != null)
            onSubAndPub.dispatch(this, aCommand);
    }

    /**
     * Override dispatch to implement a change object event handler
     */
    public interface TOnChangeObject {
        void dispatch(TEventEntry aEventEntry, int aAction, int aObjectID, String aAttribute);
    }

    /**
     * Handler to be called on receive of a change object event
     */
    public TOnChangeObject onChangeObject = null;

    /**
     * Override dispatch to implement a string event handler
     */
    public interface TOnStringEvent {
        void dispatch(TEventEntry aEventEntry, String aCommand);
    }

    /**
     * Handler to be called on receive of a string event
     */
    public TOnStringEvent onStringEvent = null;

    /**
     * Override dispatch to implement a int-string event handler
     */
    public interface TOnIntStringEvent {
        void dispatch(TEventEntry aEventEntry, int aCommand, String aCommandPayload);
    }

    /**
     * Handler to be called on receive of a int-string event
     */
    public TOnIntStringEvent onIntStringEvent = null;

    /**
     * Override dispatch to implement a tag event handler
     */
    public interface TOnTag {
        void dispatch(TEventEntry aEventEntry, int aFieldInfo, TByteBuffer aPayload);
    }

    /**
     * Handler to be called on receive of a int-string event
     */
    public TOnTag onTag = null;

    /**
     * Override dispatch to implement a stream create handler
     */
    public interface TOnStreamCreate {
        OutputStream dispatch(TEventEntry aEventEntry, String aStreamName);
    }

    /**
     * Handler to be called on receive of a stream create event
     */
    public TOnStreamCreate onStreamCreate = null;

    /**
     * Override dispatch to implement a stream end handler
     */
    public interface TOnStreamEnd {
        void dispatch(TEventEntry aEventEntry, OutputStream aStream, String aStreamName, boolean aCancel);
    }

    /**
     * Handler to be called on receive of a stream create event
     */
    public TOnStreamEnd onStreamEnd = null;

    /**
     * Override dispatch to implement a sub-and-pub handler
     */
    public interface TOnSubAndPub {
        void dispatch(TEventEntry aEventEntry, int aCommand);
    }

    /**
     * Handler to be called on receive of a sub-and-pub event
     */
    public TOnSubAndPub onSubAndPub = null;
}
