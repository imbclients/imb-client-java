package imb4;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.UUID;

public class TByteBuffer {

    // protobuf wire types
    public static final int wtVarInt = 0;          // int32, int64, uint32, uint64, sint32, sint64, bool, enum
    public static final int wt64Bit = 1;           // double or fixed int64/uint64
    public static final int wtLengthDelimited = 2; // string, bytes, embedded messages, packed repeated fields
    public static final int wtStartGroup = 3;      // deprecated
    public static final int wtEndGroup = 4;        // deprecated
    public static final int wt32Bit = 5;           // float (single) or fixed int32/uint32

    // fields
    private ByteBuffer buffer = null;

    public int remaining() {
        return buffer.remaining();
    }

    // constructor
    public TByteBuffer(int capacity) {
        buffer = ByteBuffer.allocate(capacity);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
    }

    public TByteBuffer(byte[] aValue) {
        buffer = ByteBuffer.allocate(aValue.length);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        buffer.put(aValue);
        buffer.flip();
    }

    public TByteBuffer(TByteBuffer... buffers) {
        int len = 0;
        for (TByteBuffer bb : buffers)
            len += bb.buffer.remaining();
        buffer = ByteBuffer.allocate(len);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        for (TByteBuffer bb : buffers) {
            byte[] b = new byte[bb.buffer.remaining()];
            bb.buffer.get(b, 0, b.length);
            buffer.put(b);
        }
        buffer.flip();
    }

    public int position() {
        return buffer.position();
    }

    public void position(int aValue) {
        buffer.position(aValue);
    }

    public int limit() {
        return buffer.limit();
    }

    public void limit(int aValue) {
        // check if we have to grow
        if (aValue>buffer.capacity()) {
            // allocate
            ByteBuffer new_buffer = ByteBuffer.allocate(aValue);
            new_buffer.order(ByteOrder.LITTLE_ENDIAN);
            // copy
            new_buffer.put(buffer.array());
            // reset
            new_buffer.limit(aValue);
            new_buffer.position(buffer.position());
            // swap
            buffer = new_buffer;
        }
        else
            buffer.limit(aValue);
    }

    public void put(byte[] aBuffer) {
        buffer.put(aBuffer);
    }

    public byte[] getBytes() { return buffer.array(); }

    public byte firstByte() {
        return buffer.get(0);
    }

    public void shiftLeft(byte aLeftByte) {
        int pos = buffer.position();
        // goto next position to skip first byte
        buffer.position(pos+1);
        // prepare copy buffer
        byte[] d = new byte[buffer.remaining()];
        // read remaining bytes
        buffer.get(d);
        // reset position
        buffer.position(pos);
        // rewrite new buffer (minus first byte)
        buffer.put(d);
        // set last byte
        buffer.put(aLeftByte);
        // restore position
        buffer.position(pos);
    }

    // reading
    public long bb_read_uint64() {
        int shift = 0;
        long value = 0;
        long b = buffer.get() & 0xFF;
        while (b >= 128) {
            b &= 0x7F;
            if (shift > 0)
                b <<= shift;
            shift += 7;
            value |= b;
            b = buffer.get() & 0xFF;
        }
        if (shift > 0)
            b <<= shift;
        value |= b;
        return value;

    }

    public long bb_read_int64() {
        long ui = bb_read_uint64();
        if ((ui & 1) == 1)
            return -((ui >> 1) + 1);
        else
            return ui >> 1;
    }

    public int bb_read_uint32() {
        int shift = 0;
        int value = 0;
        int b = buffer.get() & 0xFF;
        while (b >= 128) {
            b &= 0x7F;
            if (shift > 0)
                b <<= shift;
            shift += 7;
            value |= b;
            b = buffer.get() & 0xFF;
        }
        if (shift > 0)
            b <<= shift;
        value |= b;
        return value;
    }

    public int bb_read_int32() {
        int ui = bb_read_uint32();
        if ((ui & 1) == 1)
            return -((ui >> 1) + 1);
        else
            return ui >> 1;
    }

    public int bb_read_uint16() {
        return buffer.getShort();
    }

    public boolean bb_read_bool() {
        return buffer.get() != 0;
    }

    public double bb_read_double() {
        return buffer.getDouble();
    }

    public float bb_read_single() {
        return buffer.getFloat();
    }

    public UUID bb_read_guid() throws Exception {
        int len = bb_read_uint32();
        if (len == 16) {
            int D1 = buffer.getInt();
            short D2 = buffer.getShort();
            short D3 = buffer.getShort();
            byte[] D4 = new byte[8];
            buffer.get(D4);
            long msb = (((long)D1 & 0xFFFFFFFF) << 32) + (((long)D2 & 0xFFFF) << 16) + (((long)D3 & 0xFFFF));
            long lsb =
                    (((long)D4[0] & 0xFF) << 8*7) + (((long)D4[1] & 0xFF) << 8*6) +
                    (((long)D4[2] & 0xFF) << 8*5) + (((long)D4[3] & 0xFF) << 8*4) +
                    (((long)D4[4] & 0xFF) << 8*3) + (((long)D4[5] & 0xFF) << 8*2) +
                    (((long)D4[6] & 0xFF) << 8*1) + (((long)D4[7] & 0xFF) << 8*0);

            return new UUID(msb, lsb);
        } else
            throw new Exception("bb_read_guid read length!=16");
    }

    public String bb_read_string() {
        int len = bb_read_uint32(); // 64 bit officially..
        byte[] b = new byte[len];
        buffer.get(b, 0, len);
        return new String(b, 0, len, java.nio.charset.StandardCharsets.UTF_8);
    }

    public byte[] bb_read_bytes() {
        int len = bb_read_uint32(); // 64 bit officially..
        byte[] b = new byte[len];
        buffer.get(b, 0, len);
        return b;
    }

    // skip reading
    public void bb_read_skip(int aWireType) throws Exception {
        if (aWireType == wtVarInt) {
            bb_read_uint32(); // 64 bit officially..
        } else if (aWireType == wt32Bit) {
            buffer.position(buffer.position() + 4);
        } else if (aWireType == wt64Bit) {
            buffer.position(buffer.position() + 8);
        } else if (aWireType == wtLengthDelimited) {
            int len = bb_read_uint32(); // 64 bit officially,,
            buffer.position(buffer.position() + len);
        } else {
            throw new Exception("invalid wire type");
        }
    }

    public void bb_read_skip_bytes(int aNumberOfBytes) {
        buffer.position(buffer.position() + aNumberOfBytes);
    }

    static private int varint_uint64_Length(long aValue) {
        // encode in blocks of 7 bits (high order bit of byte is signal that more bytes are to follow
        // encode lower numbers directly for speed
        if (aValue < 128)
            return 1;
        else if (aValue < 128 * 128)
            return 2;
        else if (aValue < 128 * 128 * 128)
            return 3;
        else {
            // 4 bytes or more: change to dynamic size detection
            int res = 4;
            aValue >>= 7 * 4;
            while (aValue > 0) {
                res += 1;
                aValue >>= 7;
            }
            return res;
        }
    }

    static private int varint_uint32_Length(int aValue) {
        // encode in blocks of 7 bits (high order bit of byte is signal that more bytes are to follow
        // encode lower numbers directly for speed
        if (aValue < 128)
            return 1;
        else if (aValue < 128 * 128)
            return 2;
        else if (aValue < 128 * 128 * 128)
            return 3;
        else {
            // 4 bytes or more: change to dynamic size detection
            int res = 4;
            aValue >>= 7 * 4;
            while (aValue > 0) {
                res += 1;
                aValue >>= 7;
            }
            return res;
        }
    }

    // field writing
    private static void bb_put_uint64(byte[] aBuffer, int aOffset, long aValue) {
        while (aValue >= 128) {
            aBuffer[aOffset++] = (byte) ((aValue & 0x7F) | 0x80);
            aValue >>= 7;
        }
        aBuffer[aOffset] = (byte) (aValue & 0xFF);
    }

    public static void bb_put_uint32(byte[] aBuffer, int aOffset, int aValue) {
        while (aValue >= 128) {
            aBuffer[aOffset++] = (byte) ((aValue & 0x7F) | 0x80);
            aValue >>= 7;
        }
        aBuffer[aOffset] = (byte) (aValue & 0xFF);
    }

    // unsigned varint
    public static TByteBuffer bb_uint64(long aValue) {
        byte[] d = new byte[varint_uint64_Length(aValue)];
        bb_put_uint64(d, 0, aValue);
        return new TByteBuffer(d);
    }

    // unsigned varint
    public static TByteBuffer bb_uint32(int aValue) {
        byte[] d = new byte[varint_uint32_Length(aValue)];
        bb_put_uint32(d, 0, aValue);
        return new TByteBuffer(d);
    }

    // signed varint
    public static TByteBuffer bb_int64(long aValue) {
        if (aValue < 0)
            return TByteBuffer.bb_uint64(((-(aValue + 1)) << 1) | 1);
        else
            return TByteBuffer.bb_uint64(aValue << 1);
    }

    // signed varint
    public static TByteBuffer bb_int32(int aValue) {
        if (aValue < 0)
            return TByteBuffer.bb_uint32(((-(aValue + 1)) << 1) | 1);
        else
            return TByteBuffer.bb_uint32(aValue << 1);
    }

    // fixed 16 bit (cannot be tagged)
    public static TByteBuffer bb_uint16(short aValue) {
        TByteBuffer b = new TByteBuffer(2);
        b.buffer.putShort(aValue);
        b.buffer.flip();
        return b;
    }

    public static TByteBuffer bb_byte(byte aValue) {
        TByteBuffer b = new TByteBuffer(1);
        b.buffer.put(aValue);
        b.buffer.flip();
        return b;
    }

    public static TByteBuffer bb_single(float aValue) {
        TByteBuffer b = new TByteBuffer(4);
        b.buffer.putFloat(aValue);
        b.buffer.flip();
        return b;
    }

    public static TByteBuffer bb_double(double aValue) {
        TByteBuffer b = new TByteBuffer(8);
        b.buffer.putDouble(aValue);
        b.buffer.flip();
        return b;
    }

    public static TByteBuffer bb_guid(UUID aValue) {
        int len = 16;
        int lenSize = varint_uint32_Length(len);
        byte[] lenBytes = new byte[lenSize];
        TByteBuffer.bb_put_uint32(lenBytes, 0, len);
        TByteBuffer b = new TByteBuffer(lenSize + len);
        b.buffer.put(lenBytes);
        if (aValue != null) {
            long msb = aValue.getMostSignificantBits();
            long lsb  = aValue.getLeastSignificantBits();

            int D1 = (int)((msb >> 32) & 0xFFFFFFFF);
            short D2 = (short)((msb >> 16) & 0xFFFF);
            short D3 = (short)(msb & 0xFFFF);
            byte[] D4 = new byte[8];
            D4[0] = (byte)((lsb >> 8*7) & 0xFF);
            D4[1] = (byte)((lsb >> 8*6) & 0xFF);
            D4[2] = (byte)((lsb >> 8*5) & 0xFF);
            D4[3] = (byte)((lsb >> 8*4) & 0xFF);
            D4[4] = (byte)((lsb >> 8*3) & 0xFF);
            D4[5] = (byte)((lsb >> 8*2) & 0xFF);
            D4[6] = (byte)((lsb >> 8*1) & 0xFF);
            D4[7] = (byte)((lsb >> 8*0) & 0xFF);

            b.buffer.putInt(D1);
            b.buffer.putShort(D2);
            b.buffer.putShort(D3);
            b.buffer.put(D4);
        } else {
            b.buffer.putLong(0);
            b.buffer.putLong(0);
        }
        b.buffer.flip();
        return b;
    }

    public static TByteBuffer bb_string(String aValue) {
        return bb_bytes(aValue.getBytes(java.nio.charset.StandardCharsets.UTF_8));
    }

    public static TByteBuffer bb_bytes(byte[] aValue) {
        int len = aValue.length;
        int lenSize = varint_uint32_Length(len);
        byte[] lenBytes = new byte[lenSize];
        TByteBuffer.bb_put_uint32(lenBytes, 0, len);
        TByteBuffer b = new TByteBuffer(lenSize + len);
        b.buffer.put(lenBytes);
        b.buffer.put(aValue);
        b.buffer.flip();
        return b;
    }

    public static TByteBuffer bb_bytes(byte[] aValue, int aOffset, int aLimit) {
        int len = aLimit-aOffset;
        int lenSize = varint_uint32_Length(len);
        byte[] lenBytes = new byte[lenSize];
        TByteBuffer.bb_put_uint32(lenBytes, 0, len);
        TByteBuffer b = new TByteBuffer(lenSize + len);
        b.buffer.put(lenBytes);
        b.buffer.put(aValue, aOffset, len);
        b.buffer.flip();
        return b;
    }

    // taged field writing
    public static TByteBuffer bb_tag_int32(int aTag, int aValue) {
        return new TByteBuffer(TByteBuffer.bb_uint32((aTag << 3) | wtVarInt), TByteBuffer.bb_int32(aValue));
    }

    public static TByteBuffer bb_tag_uint32(int aTag, int aValue) {
        return new TByteBuffer(TByteBuffer.bb_uint32((aTag << 3) | wtVarInt), TByteBuffer.bb_uint32(aValue));
    }

    public static TByteBuffer bb_tag_int64(int aTag, long aValue) {
        return new TByteBuffer(TByteBuffer.bb_uint32((aTag << 3) | wtVarInt), TByteBuffer.bb_int64(aValue));
    }

    public static TByteBuffer bb_tag_uint64(int aTag, long aValue) {
        return new TByteBuffer(TByteBuffer.bb_uint32((aTag << 3) | wtVarInt), TByteBuffer.bb_uint64(aValue));
    }

    public static TByteBuffer bb_tag_bool(int aTag, boolean aValue) {
        if (aValue)
            return new TByteBuffer(TByteBuffer.bb_uint32((aTag << 3) | wtVarInt), TByteBuffer.bb_uint32(1));
        else
            return new TByteBuffer(TByteBuffer.bb_uint32((aTag << 3) | wtVarInt), TByteBuffer.bb_uint32(0));
    }

    public static TByteBuffer bb_tag_single(int aTag, float aValue) {
        return new TByteBuffer(TByteBuffer.bb_uint32((aTag << 3) | wt32Bit), TByteBuffer.bb_single(aValue));
    }

    public static TByteBuffer bb_tag_double(int aTag, double aValue) {
        return new TByteBuffer(TByteBuffer.bb_uint32((aTag << 3) | wt64Bit), TByteBuffer.bb_double(aValue));
    }

    public static TByteBuffer bb_tag_guid(int aTag, UUID aValue) {
        return new TByteBuffer(TByteBuffer.bb_uint32((aTag << 3) | wtLengthDelimited), TByteBuffer.bb_guid(aValue));
    }

    public static TByteBuffer bb_tag_string(int aTag, String aValue) {
        return new TByteBuffer(TByteBuffer.bb_uint32((aTag << 3) | wtLengthDelimited), TByteBuffer.bb_string(aValue));
    }

    public static TByteBuffer bb_tag_bytes(int aTag, byte[] aValue) {
        return new TByteBuffer(TByteBuffer.bb_uint32((aTag << 3) | wtLengthDelimited), TByteBuffer.bb_bytes(aValue));
    }

    public static TByteBuffer bb_tag_bytes(int aTag, byte[] aValue, int aOffset, int aLimit) {
        return new TByteBuffer(TByteBuffer.bb_uint32((aTag << 3) | wtLengthDelimited), TByteBuffer.bb_bytes(aValue, aOffset, aLimit));
    }
}
