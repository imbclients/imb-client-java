package imb4;

import java.io.*;
import java.io.File;
import java.io.FileOutputStream;


public class Main {

    public static void main(String[] args) {
        // write your code here
        try {
            TSocketConnection connection = new TSocketConnection(
                    "Java test client", 11, TConnection.imbDefaultPrefix,
                    TConnection.imbDefaultRemoteHost, TConnection.imbDefaultRemotePort);
            try {
                if (connection.getConnected()) {
                    System.out.println("connected");

                    // linking event handlers on connection
                    connection.onDisconnect = aConnection ->
                            System.out.println("disconnected..");
                    connection.onException = (aConnection, aException) ->
                            System.out.println("## exception " + aException.getMessage());

                    System.out.println("private event " + connection.getPrivateEventName());
                    System.out.println("monitor event " + connection.getMonitorEventName());

                    // subscribe to test event
                    TEventEntry event = connection.subscribe("test event");

                    // link event handlers on test event
                    event.onChangeObject = (aEventEntry, aAction, aObjectID, aAttribute) -> {
                        if ((aAction == TConnection.actionChange) && (aObjectID == 2345) && (aAttribute.compareTo("an attribute") == 0))
                            System.out.println("OK change object " + aEventEntry.getfEventName() + " " + aAction + " " + aObjectID + " " + aAttribute);
                        else
                            System.out.println("## change object " + aEventEntry.getfEventName() + " " + aAction + " " + aObjectID + " " + aAttribute);
                    };

                    event.onStringEvent = (aEventEntry, aCommand) -> {
                        if (aCommand.compareTo("string command") == 0)
                            System.out.println("OK string " + aEventEntry.getfEventName() + " " + aCommand);
                        else
                            System.out.println("## string " + aEventEntry.getfEventName() + " " + aCommand);
                    };

                    event.onIntStringEvent = (aEventEntry, aCommand, aCommandPayload) -> {
                        if (aCommand == 1234 && aCommandPayload.compareTo("int string payload") == 0)
                            System.out.println("OK int string " + aEventEntry.getfEventName() + " " + aCommand + " " + aCommandPayload);
                        else
                            System.out.println("## int string " + aEventEntry.getfEventName() + " " + aCommand + " " + aCommandPayload);
                    };

                    event.onStreamCreate = (aEventEntry, aStreamName) -> {
                        if (aStreamName.compareTo("a stream name") == 0)
                            System.out.println("OK stream create " + aEventEntry.getfEventName() + " " + aStreamName);
                        else
                            System.out.println("## stream create " + aEventEntry.getfEventName() + " " + aStreamName);
                        File f = new File("c:/temp/out.java.dmp");
                        try {
                            f.createNewFile();
                            return new FileOutputStream(f);
                        } catch (IOException e) {
                            e.printStackTrace();
                            return null;
                        }
                    };

                    event.onStreamEnd = (aEventEntry, aStream, aStreamName, aCancel) -> {
                        if (aStreamName.compareTo("a stream name") == 0)
                            System.out.println("OK stream end " + aEventEntry.getfEventName() + " " + aStreamName + " " + aCancel);
                        else
                            System.out.println("## stream end " + aEventEntry.getfEventName() + " " + aStreamName + " " + aCancel);
                    };

                    // wait for return to be pressed
                    // IMB reader thread handles in coming events while main thread is waiting for return key to be pressed
                    System.out.println(">> Press return to send events");
                    try {
                        InputStreamReader converter = new InputStreamReader(System.in);
                        BufferedReader in = new BufferedReader(converter);
                        in.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // test test events
                    event.signalString("string command");
                    event.signalIntString(1234, "int string payload");
                    File f = new File("c:/temp/test.jpg");
                    InputStream stream = new FileInputStream(f);
                    event.signalStream("a stream name", stream);
                    stream.close();
                    event.signalChangeObject(TConnection.actionChange, 2345, "an attribute");

                    // wait for return to be pressed
                    // IMB reader thread handles in coming events while main thread is waiting for return key to be pressed
                    System.out.println(">> Press return to close connection");
                    try {
                        InputStreamReader converter = new InputStreamReader(System.in);
                        BufferedReader in = new BufferedReader(converter);
                        in.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else
                    System.out.println("## NOT connected");
            } finally {
                connection.close(true);
            }
        } catch (IOException e) {
            System.out.println("## IO Exception " + e.getMessage());
        }

    }
}
