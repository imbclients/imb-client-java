package imb4;

import java.io.IOException;
import java.util.*;

public abstract class TConnection {
    // constructor
    public TConnection(String aModelName, int aModelID, String aPrefix) {
        fModelName = aModelName;
        fModelID = aModelID;
        fPrefix = aPrefix;
    }

    // destructor
    protected void finalize() throws Throwable {
        try {
            setConnected(false);
        } finally {
            super.finalize();
        }
    }

    // consts
    public static final String imbDefaultRemoteHost = "vps17642.public.cloudvps.com";
    public static final int imbDefaultRemotePort = 4004;

    public static final String imbDefaultPrefix = "ots_rt";

    public static final byte imbMagic = (byte)0xFE;

    public static final int imbMinimumPacketSize = 16;
    public static final int imbMaximumPayloadSize = 10 * 1024 * 1024;
    public static final int imbMaxStreamBodyBuffer = 8 * 1024;

    //public static final int imbSocketDefaultLingerTimeInSec = 2; // in sec

    // client state
    public static final int icsClient = 2;

    // command tags
    public static final int icehSubscribe = 2;                   // <uint32: varint>
    public static final int icehPublish = 3;                     // <uint32: varint>
    public static final int icehUnsubscribe = 4;                 // <uint32: varint>
    public static final int icehUnpublish = 5;                   // <uint32: varint>
    public static final int icehSetEventIDTranslation = 6;       // <uint32: varint>
    public static final int icehEventName = 7;                   // <string>
    public static final int icehEventID = 8;                     // <uint32: varint>

    public static final int icehUniqueClientID = 11;             // <guid>
    public static final int icehHubID = 12;                      // <guid>
    public static final int icehModelName = 13;                  // <string>
    public static final int icehModelID = 14;                    // <int32: varint> ?
    public static final int icehState = 16;                      // <uint32: varint>
    public static final int icehEventNameFilter = 17;            // <string>
    public static final int icehClose = 21;                      // <bool: varint>

    // basic event tags
    public static final int icehIntString = 1;                   // <varint>
    public static final int icehIntStringPayload = 2;          // <string>
    public static final int icehString = 3;                      // <string>
    public static final int icehChangeObject = 4;                // <int32: varint>
    public static final int icehChangeObjectAction = 5;        // <int32: varint>
    public static final int icehChangeObjectAttribute = 6;     // <string>

    public static final int icehStreamHeader = 7;                // <string> filename
    public static final int icehStreamBody = 8;                  // <bytes>
    public static final int icehStreamEnd = 9;                   // <bool> true: ok, false: cancel
    public static final int icehStreamID = 10;                 // <id: bytes/string>

    // change object actions
    public static final int actionNew = 0;
    public static final int actionDelete = 1;
    public static final int actionChange = 2;

    public static final int imbInvalidEventID = 0xFFFFFFFF;

    // fields
    protected String fModelName;
    protected int fModelID;
    protected String fPrefix;

    public final List<TEventEntry> localEventEntries = new ArrayList<>();
    private Map<Integer, TEventEntry> fRemoteEventEntries = new HashMap<>();
    public UUID uniqueClientID = null;
    public UUID hubID = null;

    /**
     * Override dispatch to implement a change object event handler
     */
    public interface TOnDisconnect {
        void dispatch(TConnection aConnection);
    }

    /**
     * Handler to be called on receive of a change object event
     */
    public TOnDisconnect onDisconnect = null;

    /**
     * Override dispatch to implement a change object event handler
     */
    public interface TOnException {
        void dispatch(TConnection aConnection, Exception aException);
    }

    /**
     * Handler to be called on receive of a change object event
     */
    public TOnException onException = null;

    // abstract methods to override to implement specific connection
    protected abstract boolean getConnected();

    protected abstract void setConnected(boolean aValue) throws IOException;

    protected abstract int readBytes(TByteBuffer aBuffer, int aOffset, int aLimit);

    public abstract void writePacket(TByteBuffer aPacket, boolean aCallCloseOnError);

    // rest methods
    public String getPrivateEventName() {
        if (uniqueClientID != null)
            return "Clients." + uniqueClientID.toString().replaceAll("-", "").toUpperCase() + ".Private";
        else
            return "";
    }

    public String getMonitorEventName() {
        if (hubID != null)
            return "Hubs." + hubID.toString().replaceAll("-", "").toUpperCase() + ".Monitor";
        else
            return "";
    }

    protected void waitForConnected(int aSpinCount) {
        try {
            while (uniqueClientID == null && aSpinCount != 0) {
                Thread.sleep(500);
                aSpinCount--;
            }
        } catch (Exception e) {}
    }

    protected void handleCommand(TByteBuffer aBuffer) {
        TEventEntry eventEntry;
//        String localEventName = "";
        int localEventID = imbInvalidEventID;
        // process tags
        try {
            while (aBuffer.remaining() > 0) {
                int fieldInfo = aBuffer.bb_read_uint32();
                switch (fieldInfo) {
                    case (icehSubscribe << 3) | TByteBuffer.wtVarInt:
                        int remoteEventID = aBuffer.bb_read_uint32();
                        eventEntry = fRemoteEventEntries.get(remoteEventID);
                        if (eventEntry != null)
                            eventEntry.handleSubAndPub(icehSubscribe);
                        break;
                    case (icehPublish << 3) | TByteBuffer.wtVarInt:
                        remoteEventID = aBuffer.bb_read_uint32();
                        eventEntry = fRemoteEventEntries.get(remoteEventID);
                        if (eventEntry != null)
                            eventEntry.handleSubAndPub(icehPublish);
                        break;
                    case (icehUnsubscribe << 3) | TByteBuffer.wtVarInt:
                        //eventName = "";
                        remoteEventID = aBuffer.bb_read_uint32();
                        eventEntry = fRemoteEventEntries.get(remoteEventID);
                        if (eventEntry != null)
                            eventEntry.handleSubAndPub(icehUnsubscribe);
                        break;
                    case (icehUnpublish << 3) | TByteBuffer.wtVarInt:
                        //eventName = "";
                        remoteEventID = aBuffer.bb_read_uint32();
                        eventEntry = fRemoteEventEntries.get(remoteEventID);
                        if (eventEntry != null)
                            eventEntry.handleSubAndPub(icehUnpublish);
                        break;
                    case (icehEventID << 3) | TByteBuffer.wtVarInt:
                        localEventID = aBuffer.bb_read_uint32();
                        break;
//                    case (icehEventName << 3) | TByteBuffer.wtLengthDelimited:
//                        localEventName = aBuffer.bb_read_string();
//                        break;
                    case (icehSetEventIDTranslation << 3) | TByteBuffer.wtVarInt:
                        remoteEventID = aBuffer.bb_read_uint32();
                        synchronized (localEventEntries) {
                            fRemoteEventEntries.remove(remoteEventID);
                            if (localEventID < localEventEntries.size()) {
                                eventEntry = localEventEntries.get(localEventID);
                                fRemoteEventEntries.put(remoteEventID, eventEntry);
                            }
                            // else local event id is invalid so invalidate remote event id -> keep removed
                        }
                        break;
                    case (icehClose << 3) | TByteBuffer.wtVarInt:
                        aBuffer.bb_read_bool();
                        close(false);
                        break;
                    case (icehHubID << 3) | TByteBuffer.wtLengthDelimited:
                        hubID = aBuffer.bb_read_guid();
                        break;
                    case (icehUniqueClientID << 3) | TByteBuffer.wtLengthDelimited:
                        uniqueClientID = aBuffer.bb_read_guid();
                        break;
                    default:
                        aBuffer.bb_read_skip(fieldInfo & 7);
                        break;
                }
            }
        } catch (Exception e) {
            onException.dispatch(this, e);
        }
    }

    protected void close(boolean aSendCloseCmd) {
        if (getConnected()) {
            if (onDisconnect != null)
                onDisconnect.dispatch(this);
            if (aSendCloseCmd)
                writeCommand(TByteBuffer.bb_tag_bool(icehClose, false), false);
            try {
                setConnected(false);
            } catch (IOException e) {}
        }
    }

    protected void signalConnectInfo(String aModelName, int aModelID) {
        writeCommand(
                new TByteBuffer(
                        TByteBuffer.bb_tag_string(icehModelName, aModelName),
                        TByteBuffer.bb_tag_int32(icehModelID, aModelID),
                        TByteBuffer.bb_tag_uint32(icehState, icsClient),
                        // bb_tag_bool(icehReconnectable, False),
                        // bb_tag_string(icehEventNameFilter, ''),
                        TByteBuffer.bb_tag_guid(icehUniqueClientID, uniqueClientID)),
                true); // trigger
    }

    protected void readPackets() {
        // read from socket and process
        TByteBuffer packet = new TByteBuffer(imbMinimumPacketSize);
        do {
            try {
                packet.position(0);
                packet.limit(imbMinimumPacketSize);
                int receivedBytes = readBytes(packet, 0,  imbMinimumPacketSize);
                if (receivedBytes == imbMinimumPacketSize) {
                    while (packet.firstByte() != imbMagic) {
                        TByteBuffer oneByte = new TByteBuffer(1);
                        if (readBytes(oneByte, 0, 1) == 1)
                            packet.shiftLeft(oneByte.firstByte());
                        else {
                            close(false);
                            break;
                        }
                    }
                    packet.bb_read_skip_bytes(1);
                    // we have magic ate the first byte
                    int size = (int) packet.bb_read_int64();
                    if (Math.abs(size)<=imbMaximumPayloadSize)
                    {
                        int extraBytesOffset = packet.limit();
                        int newLimit = packet.position() + Math.abs(size);
                        packet.limit(newLimit);
                        if (newLimit - extraBytesOffset > 0) {
                            receivedBytes = readBytes(packet, imbMinimumPacketSize, newLimit);
                            if (receivedBytes != newLimit - imbMinimumPacketSize) {
                                close(false);
                                break;
                            }
                        }
                        if (size > 0) {
                            // event
                            int eventID = packet.bb_read_uint16();
                            TEventEntry eventEntry;
                            eventEntry = fRemoteEventEntries.get(eventID);
                            if (eventEntry != null)
                                eventEntry.handleEvent(packet);
                        } else {
                            // command
                            handleCommand(packet);
                        }
                    }
                    else
                        throw new Exception("payload over size");
                } else {
                    close(false);
                    break;
                }
            } catch (Exception e) {
                if (getConnected() && onException != null)
                    onException.dispatch(this, e);
            }
        } while (getConnected());
    }

    public TEventEntry subscribe(String aEventName, boolean aUsePrefix) {
        String longEventName = aEventName;
        if (aUsePrefix)
            longEventName = fPrefix + "." + longEventName;
        String upperLongEventName = longEventName.toUpperCase();
        synchronized (localEventEntries) {
            for (TEventEntry eventEntry : localEventEntries) {
                if (eventEntry.getfEventName().toUpperCase().compareTo(upperLongEventName) == 0)
                    return eventEntry.Subscribe();
            }
            // if we come here we have not found an existing event entry
            TEventEntry newEventEntry = new TEventEntry(localEventEntries.size(), longEventName, this);
            localEventEntries.add(newEventEntry);
            return newEventEntry.Subscribe();
        }
    }

    public TEventEntry subscribe(String aEventName) {
        return subscribe(aEventName, true);
    }

    public TEventEntry publish(String aEventName, boolean aUsePrefix) {
        String longEventName = aEventName;
        if (aUsePrefix)
            longEventName = fPrefix + "." + longEventName;
        String upperLongEventName = longEventName.toUpperCase();
        synchronized (localEventEntries) {
            for (TEventEntry eventEntry : localEventEntries) {
                if (eventEntry.getfEventName().toUpperCase().compareTo(upperLongEventName) == 0)
                    return eventEntry.Publish();
            }
            // if we come here we have not found an existing event entry
            TEventEntry newEventEntry = new TEventEntry(localEventEntries.size(), longEventName, this);
            localEventEntries.add(newEventEntry);
            return newEventEntry.Publish();
        }
    }

    public TEventEntry publish(String aEventName) {
        return publish(aEventName, true);
    }

    public void writeCommand(TByteBuffer aPayload, boolean aCallCloseOnError) {
        TByteBuffer packet = new TByteBuffer(
                TByteBuffer.bb_byte(TConnection.imbMagic),
                TByteBuffer.bb_int32(-aPayload.remaining()),
                aPayload
        );
        writePacket(packet, aCallCloseOnError);
    }
}