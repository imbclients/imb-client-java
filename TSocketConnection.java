package imb4;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.net.Socket;

public class TSocketConnection extends TConnection {
    public TSocketConnection(String aModelName, int aModelID, String aPrefix, String aRemoteHost, int aRemotePort) throws IOException {
        super(aModelName, aModelID, aPrefix);
        fRemoteHost = aRemoteHost;
        fRemotePort = aRemotePort;
        setConnected(true);
    }

    // fields
    /**
     * TCP Socket the connection is based on
     */
    private Socket fSocket = null;
    /**
     * output stream linked to the socket
     */
    private OutputStream fOutputStream = null;
    /**
     * input stream linked to the socket
     */
    private InputStream fInputStream = null;
    private String fRemoteHost;
    private int fRemotePort;
    private Thread fReaderThread = null;

    protected boolean getConnected() {
        return (fSocket != null) && fSocket.isConnected();
    }

    protected void setConnected(boolean aValue) throws IOException {
        if (aValue) {
            if (!getConnected()) {
                fSocket = new Socket(fRemoteHost, fRemotePort);
                fOutputStream = fSocket.getOutputStream();
                fInputStream = fSocket.getInputStream();
                fReaderThread = new Thread(this::readPackets);
                fReaderThread.setName("imb packet reader");
                fReaderThread.start();
                // send connect info
                signalConnectInfo(fModelName, fModelID);
                // wait for unique client id as a signal that we are connected
                waitForConnected(20);
            }
        } else {
            if (getConnected()) {
                try {
                    fReaderThread.interrupt();
                    fReaderThread = null;
                    fOutputStream.close();
                    fOutputStream = null;
                    fInputStream.close();
                    fInputStream = null;
                    fSocket.close();
                    fSocket = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    protected int readBytes(TByteBuffer aBuffer, int aOffset, int aLimit) {
        try {
            int totalNumberOfBytesRead = 0;
            int localOffset = 0;
            int localLength = aLimit-aOffset;
            byte[] localBuffer = new byte[localLength];
            while (localOffset < localLength) {
                int bytesRead = fInputStream.read(localBuffer, localOffset, localLength-localOffset);
                if (bytesRead > 0) {
                    totalNumberOfBytesRead += bytesRead;
                    localOffset += bytesRead;
                }
                else
                    localOffset = localLength;
            }
            int pos = aBuffer.position();
            aBuffer.position(aOffset);
            aBuffer.put(localBuffer);
            aBuffer.position(pos); // restore position
            return totalNumberOfBytesRead;
        } catch (IOException ex) {
            return 0; // signal connection error
        }
    }

    public void writePacket(TByteBuffer aPacket, boolean aCallCloseOnError) {
        try {
            fOutputStream.write(aPacket.getBytes());
        } catch (Exception ex) {
            if (aCallCloseOnError)
                close(false);
        }
    }
}
